<?php
require_once("rolemembre.php");
$titre = "Vos favoris";
include 'header.inc.php';
include 'menumembre.php';
require_once("connpdo.php");

?>

<div class="container">
    <?php
    if (isset($_SESSION['message'])) {
        echo '<div class="alert alert-primary alert-dismissible fade show" role="alert">';
        echo $_SESSION['message'];
        echo '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
        echo '</div>';
        unset($_SESSION['message']);
    }

    // Récupérer les favoris de l'utilisateur
    $idMembre = isset($_SESSION['PROFILE']['id_user']) ? $_SESSION['PROFILE']['id_user'] : null;

    $reqFavoris = "SELECT jeux.id_jeux, jeux.nom, jeux.photo FROM favoris
                    JOIN jeux ON favoris.idJeux = jeux.id_jeux
                    WHERE favoris.idMembre = :idMembre";

    $psFavoris = $pdo->prepare($reqFavoris);
    $psFavoris->bindParam(':idMembre', $idMembre, PDO::PARAM_INT);
    $psFavoris->execute();

    $favoris = $psFavoris->fetchAll();

    if ($favoris) {
        echo '<h3>Vos favoris</h3>';
        echo '<div class="row">';
        foreach ($favoris as $favori) {
            echo '<div class="col-md-4">';
            echo '<div class="card">';
            
            // Ajout du lien vers la page de détails avec l'ID du jeu
            echo '<a href="detail_jeu.php?id=' . $favori['id_jeux'] . '">';

            echo '<img src="./images/' . $favori['photo'] . '" class="card-img-top" alt="' . $favori['nom'] . '">';
            echo '<div class="card-body">';
            echo '<h5 class="card-title">' . $favori['nom'] . '</h5>';
            echo '</div>';

            // Fermeture du lien
            echo '</a>';

            echo '</div>';
            echo '</div>';
        }
        echo '</div>';
    } else {
        echo '<p>Vous n\'avez pas encore de jeux favoris.</p>';
    }
    ?>
</div>

<?php
include 'footer.inc.php';
?>

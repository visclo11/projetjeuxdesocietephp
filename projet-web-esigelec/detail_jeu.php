<?php
require_once("rolemembre.php");
$titre = "Détails du Jeu";
include 'header.inc.php';
include 'menumembre.php';
require_once("connpdo.php");

// Récupérer l'ID du jeu depuis l'URL
$idJeu = isset($_GET['id']) ? (int)$_GET['id'] : 0;
$idMembre = isset($_SESSION['PROFILE']['id_user']) ? $_SESSION['PROFILE']['id_user'] : null;

// Utilisez $idJeu et $idMembre comme nécessaire dans votre code

if ($idJeu > 0) {
    // Récupérer les détails du jeu depuis la base de données
    $reqDetailJeu = "SELECT * FROM jeux WHERE id_jeux = :id";
    $psDetailJeu = $pdo->prepare($reqDetailJeu);
    $psDetailJeu->bindParam(':id', $idJeu, PDO::PARAM_INT);
    $psDetailJeu->execute();
    $detailJeu = $psDetailJeu->fetch();

    if ($detailJeu) {
        // Vérifier si le jeu est déjà un favori pour l'utilisateur
        $reqCheckFavori = "SELECT COUNT(*) FROM favoris WHERE idMembre = :idMembre AND idJeux = :idJeux";
        $psCheckFavori = $pdo->prepare($reqCheckFavori);
        $psCheckFavori->bindParam(':idMembre', $idMembre, PDO::PARAM_INT);
        $psCheckFavori->bindParam(':idJeux', $idJeu, PDO::PARAM_INT);
        $psCheckFavori->execute();
        $isFavori = (bool)$psCheckFavori->fetchColumn();

        // Afficher les détails du jeu
        echo '<div class="container">';
        echo '<img src="./images/' . $detailJeu['photo'] . '" class="img-fluid" alt="' . $detailJeu['nom'] . '">';
        echo '<h2>' . $detailJeu['nom'] . '</h2>';
        echo '<p>Description: ' . $detailJeu['description'] . '</p>';
        echo '<p>Catégorie: ' . $detailJeu['categorie'] . '</p>';
        echo '<p>Fichier Règles: <a href="./regles/' . $detailJeu['regles'] . '" target="_blank">Télécharger les règles</a></p>';
        
        // Ajouter le bouton "Favoris" ou "Retirer des favoris" en fonction de l'état actuel
        echo '<form method="POST" action="tt_favoris.php">';
        echo '<input type="hidden" name="id_jeux" value="' . $detailJeu['id_jeux'] . '">';
        echo '<input type="hidden" name="id_membre" value="' . $idMembre . '">';
        
        if ($isFavori) {
            echo '<button type="submit" class="btn btn-danger" name="retirer_favoris"><i class="fa fa-heart"></i> Retirer des favoris</button>';
        } else {
            echo '<button type="submit" class="btn btn-danger" name="ajouter_favoris"><i class="fa fa-heart"></i> Ajouter aux favoris</button>';
        }
        
        echo '</form>';
        
        echo '<a href="membre.php" class="btn btn-primary">Retour</a>';
        echo '</div>';
    } else {
        echo '<div class="container"><p>Aucun jeu trouvé avec cet identifiant.</p></div>';
    }
}

include 'footer.inc.php';
?>

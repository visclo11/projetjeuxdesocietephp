<?php
session_start();
require_once("connpdo.php");

if (isset($_POST['ajouter_favoris']) || isset($_POST['retirer_favoris'])) {
    $idJeux = isset($_POST['id_jeux']) ? (int)$_POST['id_jeux'] : 0;
    $idMembre = isset($_POST['id_membre']) ? (int)$_POST['id_membre'] : 0;
    
    if ($idJeux > 0 && $idMembre > 0) {
        if (isset($_POST['ajouter_favoris'])) {
            // Ajouter le jeu aux favoris
            $reqAjouterFavoris = "INSERT INTO favoris (idMembre, idJeux) VALUES (:idMembre, :idJeux)";
        } elseif (isset($_POST['retirer_favoris'])) {
            // Retirer le jeu des favoris
            $reqAjouterFavoris = "DELETE FROM favoris WHERE idMembre = :idMembre AND idJeux = :idJeux";
        }

        $psAjouterFavoris = $pdo->prepare($reqAjouterFavoris);
        $psAjouterFavoris->bindParam(':idMembre', $idMembre, PDO::PARAM_INT);
        $psAjouterFavoris->bindParam(':idJeux', $idJeux, PDO::PARAM_INT);
        $psAjouterFavoris->execute();

        // Rediriger l'utilisateur vers la page de détails du jeu après l'ajout ou le retrait des favoris
        header('Location: detail_jeu.php?id=' . $idJeux);
        exit();
    }
}

// Rediriger l'utilisateur vers une page d'erreur si quelque chose ne va pas
header('Location: erreur.php');
exit();
?>

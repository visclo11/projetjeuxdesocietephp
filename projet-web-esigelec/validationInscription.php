<?php
session_start(); // Pour les messages

// Vérifier si le formulaire a été soumis (le bouton "Valider" a été cliqué)
if (isset($_POST['valider'])) {

    // Contenu du formulaire :
    $idPartie = isset($_GET['nouPartie']) ? $_GET['nouPartie'] : '';
    $idMembre = isset($_GET['nouveauMembre']) ? $_GET['nouveauMembre'] : '';

    // Connexion :
    require_once("connpdo.php");

    // Requête SQL pour l'insertion
    $reqInsert = "INSERT INTO listemembre1(idMembres, idParties) VALUES (?, ?)";
    $psInsert = $pdo->prepare($reqInsert);

    // Vérification de la préparation de la requête
    if (!$psInsert) {
        die('Erreur de préparation de la requête d\'insertion.');
    }

    // Liaison des paramètres pour l'insertion
    $psInsert->bindParam(1, $idMembre, PDO::PARAM_INT);
    $psInsert->bindParam(2, $idPartie, PDO::PARAM_INT);

    // Exécution de la requête d'insertion
    $resultatInsert = $psInsert->execute();

    // Vérification du résultat de l'exécution
    if (!$resultatInsert) {
        die('Erreur d\'exécution de la requête d\'insertion.');
    }

    // Ajout de la requête UPDATE pour mettre à jour le statut (adaptez selon vos besoins)
    $nouveauStatut = 'Valider'; // Remplacez par la valeur souhaitée
    $idMembreToUpdate = $idMembre; // Remplacez par l'identifiant du membre à mettre à jour

    $reqUpdate = "UPDATE listemembre1 SET Statut = ? WHERE idMembres = ?";
    $psUpdate = $pdo->prepare($reqUpdate);

    // Vérification de la préparation de la requête d'update
    if (!$psUpdate) {
        die('Erreur de préparation de la requête d\'update.');
    }

    // Liaison des paramètres pour l'update
    $psUpdate->bindParam(1, $nouveauStatut, PDO::PARAM_STR);
    $psUpdate->bindParam(2, $idMembreToUpdate, PDO::PARAM_INT);

    // Exécution de la requête d'update
    $resultatUpdate = $psUpdate->execute();

    // Vérification du résultat de l'exécution de l'update
    if (!$resultatUpdate) {
        die('Erreur d\'exécution de la requête d\'update.');
    }

    // Redirection vers la page d'accueil par exemple :
    header('Location: inscriptionPartie.php');
}
?>


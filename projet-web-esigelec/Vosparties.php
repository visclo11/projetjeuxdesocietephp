<?php
    require_once("rolemembre.php");
    $titre = "Recherche de parties";
    include 'header.inc.php';
    include 'menumembre.php';

    // Connexion à la base de données
    require_once("connpdo.php");

    // Vérifier si la clé 'id_user' est définie dans $_SESSION
    $membre_id = isset($_SESSION['PROFILE']['id_user']) ? $_SESSION['PROFILE']['id_user'] : null;

    if ($membre_id !== null) {
        // Requête pour récupérer les parties auxquelles l'utilisateur est inscrit
        $reqPartiesInscrites = "SELECT partie.*, jeux.nom AS nom_jeu
                                FROM partie
                                INNER JOIN listemembre ON partie.idParties = listemembre.idParties
                                INNER JOIN jeux ON partie.idJeux = jeux.id_jeux
                                WHERE listemembre.idMembre = ?";
        $psPartiesInscrites = $pdo->prepare($reqPartiesInscrites);
        $psPartiesInscrites->execute([$membre_id]);
    }
?>

<div class="container">
    <?php if ($membre_id !== null) : ?>
        <!-- Afficher les parties auxquelles l'utilisateur est inscrit -->
        <h2>Parties auxquelles vous êtes inscrit</h2>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Date</th>
                    <th scope="col">Heure</th>
                    <th scope="col">Jeu</th>
                    <th scope="col">Supprimer</th> 
                    
                </tr>
            </thead>
            <tbody>
                <?php
                while ($rowPartieInscrite = $psPartiesInscrites->fetch()) {
                    echo '<tr>';
                    echo '<td>' . date('d/m/Y', strtotime($rowPartieInscrite['date'])) . '</td>';
                    echo '<td>' . $rowPartieInscrite['heure'] . '</td>';
                    echo '<td>' . $rowPartieInscrite['nom_jeu'] . '</td>';
                    

                    // Bouton de désinscription
                    echo '<td>';
                    echo '<form action="tt_desinscriptionPartie.php" method="POST">';
                    echo '<input type="hidden" name="partie_id" value="' . $rowPartieInscrite['idParties'] . '">';
                    echo '<input type="hidden" name="membre_id" value="' . $membre_id . '">';
                    echo '<button type="submit" class="btn btn-danger" name="desinscription_btn">Se désinscrire</button>';
                    echo '</form>';
                    echo '</td>';

                    echo '</tr>';
                }
                ?>
            </tbody>
        </table>
    <?php else : ?>
        <p>Vous n'êtes inscrit à aucune partie pour le moment.</p>
    <?php endif; ?>
</div>

<?php
    include 'footer.inc.php';
?>

<?php
session_start();
    $titre = "Connexion";
    include 'header.inc.php';
    include 'menu.inc.php';
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Page de Connexion</title>
    <style>
        body {
            background-image: url('images/connexion.jpeg'); /* Remplacez 'votre-image.jpg' par le chemin de votre image */
            background-size: cover; /* Pour couvrir l'ensemble de la page */
            background-position: center; /* Pour centrer l'image */
            background-repeat: no-repeat; /* Pour éviter la répétition de l'image */
            margin: 0; /* Supprimer la marge par défaut du body */
            padding: 0; /* Supprimer le padding par défaut du body */
            font-family: Arial, sans-serif; /* Votre police de caractères */
            color: #fff; /* Couleur du texte pour contraster avec l'image de fond */
        }

        .container {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            height: 100vh;
        }

        .login-container {
            background-color: rgba(255, 255, 255, 0.8);
            padding: 20px;
            border-radius: 8px;
            width: 50%;
        }

        h1 {
            color: #fff;
        }

        label {
            color: #000;
        }

        .btn-outline-primary {
            color: #007bff;
            border-color: #007bff;
        }

        .btn-outline-primary:hover {
            color: #fff;
            background-color: #007bff;
            border-color: #007bff;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="login-container">
            <h1>Connexion</h1>
            <?php
            if (isset($_SESSION['message'])) {
                if ($_SESSION['message'] == "Erreur de connexion") {
                    echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">';
                } else {
                    echo '<div class="alert alert-primary alert-dismissible fade show" role="alert">';
                }
                echo $_SESSION['message'];
                echo '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
                echo '</div>';
                unset($_SESSION['message']);
            }
            ?>
            <form method="POST" action="tt_connexion.php">
                <div class="container">
                    <div class="row my-3">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="email" class="form-label">Email</label>
                                <input type="email" class="form-control " id="email" name="email" placeholder="Votre email..." required>
                            </div>
                            <div class="col-md-6">
                                <label for="password" class="form-label">Mot de passe</label>
                                <input type="password" class="form-control " id="password" name="password" placeholder="Votre mot de passe..." required>
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="d-grid gap-2 d-md-block text-center"><button class="btn btn-outline-primary" type="submit">Connexion</button></div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <?php
    include 'footer.inc.php';
    ?>
</body>

</html>
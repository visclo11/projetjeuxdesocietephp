<?php
    require_once("roleadmin.php");
    $titre = "Inscription";
    include 'header.inc.php';
    include 'menuadmin.php';
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Page d'Inscription</title>
    <style>
        body {
            background-image: url('images/Administrator.jpeg');
            background-size: cover;
            background-position: center;
            background-repeat: no-repeat;
            margin: 0;
            padding: 0;
            font-family: Arial, sans-serif;
            color: #fff;
        }

        .container {
            display: flex;
            flex-direction: column;
            justify-content: flex-start; /* Ajustez la position verticale selon vos besoins */
            align-items: center;
            min-height: 100vh;
        }

        .registration-container {
            background-color: rgba(255, 255, 255, 0.8);
            padding: 20px;
            border-radius: 8px;
            width: 40%; /* Ajustez la largeur selon vos besoins */
            margin-left: auto;
            margin-right: auto;
            margin-top: 20px; /* Ajustez la marge supérieure selon vos besoins */
        }

        h1 {
            color: #fff;
            margin-bottom: 20px; /* Ajustez la marge inférieure selon vos besoins */
        }

        label {
            color: #000;
        }

        .btn-outline-primary {
            color: #007bff;
            border-color: #007bff;
            text-align: center; /* Pour centrer le texte du bouton */
        }

        .btn-outline-primary:hover {
            color: #fff;
            background-color: #007bff;
            border-color: #007bff;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="registration-container">
            <h1>Inscription</h1>
            <?php
            if (isset($_SESSION['message'])) {
                if ($_SESSION['message'] == "Erreur d'inscription") {
                    echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">';
                } else {
                    echo '<div class="alert alert-primary alert-dismissible fade show" role="alert">';
                }
                echo $_SESSION['message'];
                echo '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
                echo '</div>';
                unset($_SESSION['message']);
            }
            ?>
            <form method="POST" action="tt_ajouteradmin.php">
                <div class="container">
                    <div class="mb-3">
                        <label for="nom" class="form-label">Nom</label>
                        <input type="text" class="form-control" id="nom" name="nom" placeholder="Son nom..." required>
                    </div>

                    <div class="mb-3">
                        <label for="prenom" class="form-label">Prénom</label>
                        <input type="text" class="form-control" id="prenom" name="prenom" placeholder="Son prénom..." required>
                    </div>

                    <div class="mb-3">
                        <label for="email" class="form-label">Email</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Son email..." required>
                    </div>

                    <div class="mb-3">
                        <label for="password" class="form-label">Mot de passe</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Son mot de passe..." required>
                    </div>

                    <div class="mb-3">
                        <button class="btn btn-outline-primary" type="submit">Inscription</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <?php
    include 'footer.inc.php';
    ?>
</body>

</html>

<?php
if (isset($_SESSION['PROFILE'])) {
?>

<nav class="navbar navbar-expand-md bg-dark border-bottom border-body" data-bs-theme="dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Membre</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarText">
      <ul class="navbar-nav me-auto mb-lg-0">
        <li class="nav-item">
          <a class="nav-link" href="membre.php">Accueil</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="VosFavoris.php">Vos favoris</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Vos parties
          </a>
          <ul class="dropdown-menu">
            <li class="nav-item">
              <a class="nav-link" href="inscriptionpartie.php">Inscription parties</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="Vosparties.php">Vos parties</a>
            </li>
          </ul>
        </li>
      </ul>
      <ul class="navbar-nav mb-lg-0 ms-auto">
        <li class="nav-item">
          <a class="nav-link" href="logout.php">Déconnexion</a>
        </li>
      </ul>
    </div>
  </div>
</nav>

<?php
}
?>

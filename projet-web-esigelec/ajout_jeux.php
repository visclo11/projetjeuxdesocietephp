<?php
require_once("roleadmin.php");
$titre = "Ajout Jeux";
include 'header.inc.php';
include 'menuadmin.php';
?>
<div class="container">
    <h1>Ajout d'un Jeu</h1>
    <form method="POST" action="tt_jeux.php" enctype="multipart/form-data">
        <div class="container">
            <div class="row my-3">
                <div class="col-md-6">
                    <label for="nomjeux" class="form-label">Nom du jeu</label>
                    <input type="text" class="form-control" id="nomjeux" name="nomjeux" placeholder="Nom du jeu..." required>
                </div>

                <div class="col-md-6">
               
                    <label for="categorie" class="form-label">Catégorie du jeu</label>
                    <input type="text" class="form-control" id="categorie" name="categorie" placeholder="Catégorie du jeu..." required>
                </div>
            </div>
                    

        <div class="row my-3">
            <div class="col-md-6">
              <label for="description" class="form-label">Description du jeu</label>
              <textarea class="form-control" id="description" name="description" placeholder="Description du jeu..." style="resize: none;" rows="6"></textarea>
            </div>

            <div class="col-md-6">
             <label  class="form-label">Ajout d'une photo </label>
             <input type="file" name="userfile" class="form-control" />
 
        </div>

                <div class="col-md-6">
                    <label for="regles" class="form-label">Règles du jeu (PDF)</label>
                    <input type="file" name="regles" class="form-control" />
                </div>
            </div>
        </div>

            <div class="row my-3">
                <div class="d-grid gap-2 d-md-block">
                    <button class="btn btn-outline-primary" type="submit">Ajouter</button>
                </div>
            </div>
        </div>
    </form>
</div>
<?php
include 'footer.inc.php';
?>

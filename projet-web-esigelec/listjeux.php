<?php
        
    

    require_once("roleadmin.php");
    $titre = "Liste des jeux";
    include 'header.inc.php';
    include 'menuadmin.php';

?>
<div class="container">
<h1>List des jeux</h1>



<table class="table">
  <thead>
    <tr>
      <th scope="col">IDJEUX</th>
      <th scope="col">Nom</th>
      <th scope="col">Categorie</th>
      <th scope="col">Photo</th>
      <th scope="col">Action</th>
      <th scope="col">Update</th>
  </thead>
  <tbody>
  
  <?php

// Connexion :
require_once("connpdo.php");
$req = "SELECT id_jeux, nom, categorie, description, regles, photo FROM jeux";
$ps = $pdo->prepare($req);
$ps->execute();

while ($row = $ps->fetch()) {
    echo '<tr>';
    echo '<th scope="row">' . $row['id_jeux'] . '</th>';
    echo '<td>' . $row['nom'] . '</td>';
    echo '<td>' . $row['categorie'] . '</td>';
    echo '<td><img src="images/' . $row['photo'] . '" width="100px" height="100px"></td>';
    echo '<td><a href="tt_supprimjeux.php?nom=' . $row['nom'] . '">Supprimer</a></td>';
    echo '<td><a href="modfierjeux.php?ancienJeu=' . urlencode($row['id_jeux']) . 
         '&nouveauNom=' . urlencode($row['nom']) . 
         '&nouvelleCategorie=' . urlencode($row['categorie']) . 
         '&nouvelleDescription=' . urlencode($row['description']) . 
         '&regles=' . urlencode($row['regles']) . 
         '&photo=' . urlencode($row['photo']) . '">Modifier</a></td>';
    echo '</tr>';
}



?>

</tbody>

</table>


</div>
<?php
    include 'footer.inc.php';
?>

<?php
require_once("rolemembre.php");
$titre = "Recherche de parties";
include 'header.inc.php';
include 'menumembre.php';

// Connexion à la base de données
require_once("connpdo.php");

// Requête pour récupérer tous les jeux
$reqJeux = "SELECT * FROM jeux";
$psJeux = $pdo->prepare($reqJeux);
$psJeux->execute();

// Variable pour stocker l'ID du jeu sélectionné
$selectedJeuId = null;

// Vérifier si un jeu est sélectionné
if (isset($_GET['jeu'])) {
    $selectedJeuId = $_GET['jeu'];

    // Requête pour récupérer les parties associées au jeu sélectionné
    $reqParties = "SELECT * FROM partie WHERE idJeux = ?";
    $psParties = $pdo->prepare($reqParties);
    $psParties->execute([$selectedJeuId]);
}
?>

<div class="container">
    <h1 class="mt-4">Recherche de parties</h1>

    <!-- Liste des jeux -->
    <div class="row">
        <div class="col-md-6">
            <h2>Choisissez un jeu</h2>
            <div class="list-group">
                <?php
                // Afficher la liste des jeux
                while ($rowJeu = $psJeux->fetch()) {
                    $isSelected = ($rowJeu['id_jeux'] == $selectedJeuId) ? 'active' : '';
                    echo '<a href="?jeu=' . $rowJeu['id_jeux'] . '" class="list-group-item list-group-item-action ' . $isSelected . '">';
                    echo $rowJeu['nom'];
                    echo '</a>';
                }
                ?>
            </div>
        </div>

        <!-- Afficher les parties associées au jeu sélectionné -->
        <?php if ($selectedJeuId !== null) : ?>
            <div class="col-md-6">
                <h2>Parties pour le jeu sélectionné</h2>
                <table class="table">
                    <thead>
                        <link rel="stylesheet" href="styles.css">
                        <tr>
                            <th scope="col">Date</th>
                            <th scope="col">Heure</th>
                            <th scope="col">Inscription</th>
                            <th scope="col">Nombre d'inscrit</th> <!-- Nouvelle colonne -->
                            <!-- Ajoutez d'autres colonnes au besoin -->
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        // Vérifier si la clé 'id_user' est définie dans $_SESSION
                        $membre_id = isset($_SESSION['PROFILE']['id_user']) ? $_SESSION['PROFILE']['id_user'] : null;

                        // Afficher les parties associées
                        while ($rowPartie = $psParties->fetch()) {
                            // Vérifier si l'utilisateur est déjà inscrit
                            $reqInscription = "SELECT COUNT(*) AS total FROM listemembre WHERE idMembre = ? AND idParties = ?";
                            $psInscription = $pdo->prepare($reqInscription);
                            $psInscription->execute([$membre_id, $rowPartie['idParties']]);
                            $resultInscription = $psInscription->fetch();

                            // Compter le nombre de membres inscrits
                            $reqNombreMembres = "SELECT COUNT(*) AS totalMembres FROM listemembre WHERE idParties = ?";
                            $psNombreMembres = $pdo->prepare($reqNombreMembres);
                            $psNombreMembres->execute([$rowPartie['idParties']]);
                            $totalMembres = $psNombreMembres->fetchColumn();

                            echo '<tr>';
                            echo '<td>' . date('d/m/Y', strtotime($rowPartie['date'])) . '</td>';
                            echo '<td>' . $rowPartie['heure'] . '</td>';
                            echo '<td>';
                            // Vérifier l'inscription de l'utilisateur
                            if ($resultInscription['total'] == 0) {
                                // Utilisateur non inscrit, afficher le bouton d'inscription
                                echo '<form action="tt_inscriptionPartie.php" method="POST">';
                                echo '<input type="hidden" name="partie_id" value="' . $rowPartie['idParties'] . '">';
                                echo '<input type="hidden" name="membre_id" value="' . $membre_id . '">';
                                echo '<button type="submit" class="btn btn-success" name="inscription_btn">S\'inscrire</button>';
                                echo '</form>';
                            } else {
                                // Utilisateur déjà inscrit, afficher un message ou une autre indication
                                echo '<button class="btn btn-danger" disabled>Déjà inscrit</button>';
                            }
                            echo '</td>';
                            echo '<td>';
                            // Afficher le nombre de membres inscrits et le bouton "Afficher" pour les détails
                            echo '<span class="total-membres">' . $totalMembres . '</span>';
                            echo '<button class="btn btn-link btn-show-members" data-partie-id="' . $rowPartie['idParties'] .'">Afficher</button>';
                            echo '<div class="list-members" data-partie-id="' . $rowPartie['idParties'] .'"></div>';
                            echo '</td>';
                            // Ajoutez d'autres colonnes au besoin
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        <?php endif; ?>
    </div>
</div>

<?php
include 'footer.inc.php';
?>

<!-- Ajouter le script JavaScript pour afficher les membres au clic sur le bouton "Afficher" -->

    <script>
    document.addEventListener('DOMContentLoaded', function () {
        var showMembersButtons = document.querySelectorAll('.btn-show-members');

        showMembersButtons.forEach(function (button) {
            button.addEventListener('click', function () {
                var partieId = button.getAttribute('data-partie-id');
                var listMembers = document.querySelector('.list-members[data-partie-id="' + partieId + '"]');

                // Si la liste est visible, la cacher; sinon, l'afficher
                if (listMembers.classList.contains('visible')) {
                    listMembers.classList.remove('visible');
                } else {
                    // Effectuez la requête pour récupérer les membres
                    fetch('tt_getMembers.php?partie_id=' + partieId)
                        .then(response => response.json())
                        .then(data => {
                            // Affichez les membres dans une liste déroulante
                            listMembers.innerHTML = '<ol class="list-group list-group-numbered">' +
                                data.members.map(member => '<li class="list-group-item">' + member + '</li>').join('') +
                                '</ol>';
                            listMembers.classList.add('visible');
                        })
                        .catch((error) => {
                            console.error('Error:', error);
                        });
                }
            });
        });
    });
</script>




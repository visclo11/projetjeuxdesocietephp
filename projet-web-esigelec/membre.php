<?php
require_once("rolemembre.php");
$titre = "Membre";
include 'header.inc.php';
include 'menumembre.php';
require_once("connpdo.php");

echo '<head>';
echo '<link rel="stylesheet" type="text/css" href="styles.css">';
echo '</head>';

// Pagination
$jeuxParPage = 5; // Nombre de jeux par page
$pageCourante = isset($_GET['page']) ? (int)$_GET['page'] : 1; // Page actuelle

// Calculer l'offset pour la requête SQL
$offset = ($pageCourante - 1) * $jeuxParPage;

// Récupérer la liste des jeux depuis la base de données
$reqJeux = "SELECT * FROM jeux LIMIT :jeuxParPage OFFSET :offset";
$psJeux = $pdo->prepare($reqJeux);
$psJeux->bindParam(':jeuxParPage', $jeuxParPage, PDO::PARAM_INT);
$psJeux->bindParam(':offset', $offset, PDO::PARAM_INT);
$psJeux->execute();
$jeux = $psJeux->fetchAll();

// Récupérer le nombre total de jeux pour la pagination
$reqTotalJeux = "SELECT COUNT(*) as total FROM jeux";
$totalJeux = $pdo->query($reqTotalJeux)->fetchColumn();

// Calculer le nombre total de pages
$nombrePages = ceil($totalJeux / $jeuxParPage);

// Styles CSS pour réduire la taille des cartes
$cardWidth = 200; // Largeur maximale de la carte en pixels
$cardMargin = 10; // Marge entre les cartes en pixels

// Afficher la liste des jeux
echo '<div class="container">';
foreach ($jeux as $rowJeu) {
    echo '<div class="card" style="max-width: ' . $cardWidth . 'px; margin-right: ' . $cardMargin . 'px;">';
    $imagePath = "./images/" . $rowJeu['photo'];
    $width = 150; // Définir la largeur souhaitée de l'image
    $height = 100; // Définir la hauteur souhaitée de l'image
    echo '<img src="' . $imagePath . '" class="card-img-top img-thumbnail" alt="' . $rowJeu['nom'] . '" width="' . $width . '" height="' . $height . '">';
    echo '<div class="card-body">';
    echo '<h5 class="card-title">' . $rowJeu['nom'] . '</h5>';
    echo '<p class="card-text">' . $rowJeu['description'] . '</p>';
    echo '<a href="detail_jeu.php?id=' . $rowJeu['id_jeux'] . '" class="btn btn-primary">Voir plus</a>';
    echo '</div>';
    echo '</div>';
}
echo '</div>';

// Afficher la pagination centrée en bas de la page
echo '<nav aria-label="Page navigation example" class="fixed-bottom">';
echo '<ul class="pagination justify-content-center">';
for ($i = 1; $i <= $nombrePages; $i++) {
    echo '<li class="page-item ' . ($i == $pageCourante ? 'active' : '') . '"><a class="page-link" href="?page=' . $i . '">' . $i . '</a></li>';
}
echo '</ul>';
echo '</nav>';

include 'footer.inc.php';
?>

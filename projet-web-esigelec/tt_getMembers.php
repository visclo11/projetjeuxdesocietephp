<?php
// Connexion à la base de données
require_once("connpdo.php");

// Vérifier si l'ID de la partie est fourni dans la requête
if (isset($_GET['partie_id'])) {
    $partieId = $_GET['partie_id'];

    // Requête pour récupérer les membres inscrits à la partie avec leurs noms et prénoms
    $reqMembers = "SELECT u.nom, u.prenom
    FROM listemembre lm
    JOIN user u ON lm.idMembre = u.id_user
    WHERE lm.idParties = ?";
    $psMembers = $pdo->prepare($reqMembers);
    $psMembers->execute([$partieId]);

    // Récupérer les résultats dans un tableau
    $members = [];
    while ($rowMember = $psMembers->fetch()) {
        $members[] = $rowMember['prenom'] . ' ' . $rowMember['nom'];
    }

    // Retourner les membres au format JSON
    echo json_encode(['members' => $members]);
} else {
    // Si l'ID de la partie n'est pas fourni, retourner une erreur
    echo json_encode(['error' => 'ID de la partie non fourni']);
}
?>


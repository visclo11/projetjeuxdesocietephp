<?php
require_once("roleadmin.php");
$titre = "Liste des membres et des jeux";
include 'header.inc.php';
include 'menuadmin.php';
include 'param.inc.php'
?>


<div class="container">
    <h1>Créneaux des jeux</h1>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">IDJEUX</th>
                <th scope="col">IDPARTIE</th>
                <th scope="col">NOMJEUX</th>
                <th scope="col">DATE</th>
                <th scope="col">HEURE</th>
                <th scope="col">NOMBRE DE PARTICIPANT</th>
                <th scope="col">ACTION</th>
            </tr>
        </thead>
        <tbody>
            <?php
            // Connexion :
            require_once("connpdo.php");
            $req = "SELECT partie.idJeux, partie.idParties, jeux.nom, partie.date, partie.heure  FROM partie JOIN jeux ON partie.idjeux = jeux.id_jeux";
            $ps = $pdo->prepare($req);
            $ps->execute();

            while ($row = $ps->fetch()) {
                echo '<tr>';
                echo '<th scope="row">' . $row['idJeux'] . '</th>';
                echo '<td>' . $row['idParties'] . '</td>';
                echo '<td>' . $row['nom'] . '</td>';
                echo '<td>' . $row['date'] . '</td>';
                echo '<td>' . $row['heure'] . '</td>';

                $idParties = $row['idParties'];

                // Utilisation d'une requête préparée avec un paramètre
                $stmt = $pdo->prepare("SELECT COUNT(*) AS nombre_elements FROM listemembre WHERE idParties = :idParties");
                $stmt->bindParam(':idParties', $idParties, PDO::PARAM_INT);
                $stmt->execute();
                $result = $stmt->fetch(PDO::FETCH_ASSOC);

                // Obtenez la valeur de 'nombre_elements' depuis le résultat de la requête
                if ($result && isset($result['nombre_elements'])) {
                    $nombre_elements = $result['nombre_elements'];
                    echo '<td>' . $nombre_elements . '</td>';
                } else {
                    echo '<td>Erreur lors de la récupération des données</td>';
                }

                // Fermez la requête préparée
                $stmt->closeCursor();

                echo '<td><a href="tt_supprimerCreneaux.php?idJeux=' . $row['idJeux'] . '">Supprimer</a></td>';
                echo '</tr>';
            }
            
            ?>
        </tbody>
    </table>
</div>
<?php
include 'footer.inc.php';
?>





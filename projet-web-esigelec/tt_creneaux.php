<?php
session_start(); // Pour les messages

// Contenu du formulaire :
$date = htmlentities($_POST['DATE']);
$heure = htmlentities($_POST['HEURE']);
$nomjeux = htmlentities($_POST['nom']);

// Option pour bcrypt
$options = [
    'cost' => 12,
];

// Connexion :
require_once("param.inc.php");
$mysqli = new mysqli($host, $login, $passwd, $dbname);
if ($mysqli->connect_error) {
    die('Erreur de connexion (' . $mysqli->connect_errno . ') ' . $mysqli->connect_error);
}

// Attention, ici on ne vérifie pas si l'utilisateur existe déjà
if ($stmt = $mysqli->prepare("SELECT id_jeux FROM jeux WHERE nom= ? ")) {
    $stmt->bind_param("s", $nomjeux);
    $stmt->execute();
    $result = $stmt->get_result();
    
    // Assurez-vous qu'une seule ligne est récupérée
    if ($result->num_rows == 1) {
        $row = $result->fetch_assoc();
        $nbjeux = $row['id_jeux'];

        // Insertion dans la table "partie"
        if ($stmtInsert = $mysqli->prepare("INSERT INTO partie(idJeux, date, heure) VALUES (?, ?, ?)")) {
            $stmtInsert->bind_param("iss", $nbjeux, $date, $heure);
            
            // Le message est mis dans la session, il est préférable de séparer message normal et message d'erreur.
            if ($stmtInsert->execute()) {
                $_SESSION['message'] = "Enregistrement réussi";
            } else {
                $_SESSION['message'] = "Impossible d'enregistrer";
            }

            $stmtInsert->close();
        } else {
            $_SESSION['message'] = "Erreur de préparation de la requête d'insertion";
        }
    } else {
        $_SESSION['message'] = "Le jeu n'a pas été trouvé";
    }

    $stmt->close();
} else {
    $_SESSION['message'] = "Erreur de préparation de la requête de recherche du jeu";
}

$mysqli->close();

// Redirection vers la page d'accueil par exemple :
header('Location: index.php');
?>

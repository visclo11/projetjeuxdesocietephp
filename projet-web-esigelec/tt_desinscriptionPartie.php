<?php
session_start(); // Pour les messages

// Contenu du formulaire :
$idPartie =  htmlentities($_POST['partie_id']);
$idMembre =  htmlentities($_POST['membre_id']);

// Connexion :
require_once("connpdo.php");

// Requête SQL pour l'insertion
$reqDesinscription = "DELETE FROM listemembre WHERE idMembre = ? AND idParties = ?";
        $psDesinscription = $pdo->prepare($reqDesinscription);
        

// Vérification de la préparation de la requête
if (!$psDesinscription) {
    die('Erreur de préparation de la requête.');
}

// Liaison des paramètres
$psDesinscription->bindParam(1, $idMembre, PDO::PARAM_INT);
$psDesinscription->bindParam(2, $idPartie, PDO::PARAM_INT);

// Exécution de la requête
$resultat = $psDesinscription->execute();

// Vérification du résultat de l'exécution
if (!$resultat) {
    die('Erreur d\'exécution de la requête.');
}

// Redirection vers la page d'accueil par exemple :
header('Location: Vosparties.php');
?>
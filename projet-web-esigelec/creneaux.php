<?php
   require_once("roleadmin.php");
    $titre = "definitioncreneaux";
    include 'header.inc.php';
    include 'menuadmin.php';
?>
<div class="container">
<h1>Connexion</h1>
<form  method="POST" action="tt_creneaux.php">
    <div class="container">
      <div class="row my-5">
        <div class="col-md-4">
            <label for="date" class="form-label">DATE</label>
            <input type="date" class="form-control " id="date" name="DATE" placeholder="La date..." required>
        </div>
      </div>
     <div class="row my-5">
        <div class="col-md-4">
            <label for="heure">Heure (HH:MM):</label>
            <input type="text" id="heure" name="HEURE" pattern="[0-9]{2}:[0-9]{2}" placeholder="HH:MM" required>
        </div>
    </div>
    <div class="row my-5">
        <div class="col-md-4">
            <label for="Jeux" class="form-label">NOM JEUX</label>
            <input type="text" class="form-control " id="nomjeux" name="nom"  placeholder="Nom du jeux..." required>
        </div>
    </div>
    <div class="row my-3">
        <div class="d-grid gap-2 d-md-block"><button class="btn btn-outline-primary" type="submit">SOUMETTRE</button></div>   
        </div>

   </div>
</form>
</div>

<?php
    include 'footer.inc.php';
?>
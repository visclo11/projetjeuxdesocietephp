<nav class="navbar navbar-expand-md bg-dark border-bottom border-body" data-bs-theme="dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Admin</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarText">
      <ul class="navbar-nav me-auto mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="admin.php">Accueil</a>
        </li>
       
        <?php
        if((isset($_SESSION['PROFILE'])))
        {
          $nom=$_SESSION['PROFILE']['nom'];
          echo'<li class="nav-item">';
          echo'<a class="nav-link" href="chez.php">Chez '.$nom.'</a>';
          echo'</li>';
         
        }
        ?>
      </ul>
      <ul class="navbar-nav mb-lg-0">
        <li class="nav-item">
          <a class="nav-link" href="logout.php">Déconnexion</a>
        </li>

      
      </ul>


    </div>
  </div>
</nav>
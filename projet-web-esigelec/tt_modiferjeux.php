<?php
session_start(); // Pour les messages

$ancienJeu = htmlentities($_POST['id_jeux']); // Nom d'avant la modification
$nouveauNom = $_POST['nom']; // Nouveau nom
$nouvelleCategorie = $_POST['categorie']; // Nouvelle categorie
$nouvelleDescription = $_POST['description']; // Nouvelle description

$regles = $_FILES['regles']['name'];
$reglesTemp = $_FILES['regles']['tmp_name'];
move_uploaded_file($reglesTemp, './regles/' . $regles);

$photo = $_FILES['userfile']['name'];
$fichierTemp = $_FILES['userfile']['tmp_name'];
move_uploaded_file($fichierTemp, './images/' . $photo);

require_once("connpdo.php");

// Vous devez spécifier les valeurs des champs que vous souhaitez mettre à jour (NOM et FILE) dans la requête.
$req = "UPDATE jeux SET nom=?, categorie=?, description=?, regles=?, photo=? WHERE id_jeux=?";
$ps = $pdo->prepare($req);
$params = array($nouveauNom, $nouvelleCategorie, $nouvelleDescription, $regles, $photo, $ancienJeu);

if ($ps->execute($params)) {
    $_SESSION['message'] = "Modification réussie.";
    header("location: listjeux.php");
} else {
    $_SESSION['message'] = "Problème de modification.";
    header("location: listjeux.php");
}
?>

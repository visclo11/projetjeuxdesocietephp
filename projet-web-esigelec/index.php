

</style>

<?php
session_start();
$titre = "Accueil";
include 'header.inc.php';
include 'menu.inc.php';
require_once("connpdo.php");

// Récupérer la liste des jeux depuis la base de données
$reqJeux = "SELECT * FROM jeux";
$psJeux = $pdo->prepare($reqJeux);
$psJeux->execute();
$jeux = $psJeux->fetchAll(); // Stocker les résultats dans un tableau
?>

<div class="container mt-4">
    <!-- Carousel pour les jeux de société -->
    <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel" data-bs-interval="3000">

        <div class="carousel-indicators">
            <?php
            $firstGame = true;
            $indicatorCount = 0;
            foreach ($jeux as $rowJeu) {
                echo '<button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="' . $indicatorCount . '" class="' . ($firstGame ? 'active' : '') . '" aria-label="Slide ' . ($indicatorCount + 1) . '"></button>';
                $indicatorCount++;
                $firstGame = false;
            }
            ?>
        </div>
        <div class="carousel-inner">
            <?php
            $firstGame = true;
            $gamesToShow = 3; // Nombre de jeux à afficher
            $count = 0;
            foreach ($jeux as $rowJeu) {
                if ($count >= $gamesToShow) {
                    break; // Sortir de la boucle après avoir affiché le nombre de jeux souhaité
                }

                echo '<div class="carousel-item ' . ($firstGame ? 'active' : '') . '">';
                echo '<img src="./images/' . $rowJeu['photo'] . '" class="d-block mx-auto w-30 h-50" alt="' . $rowJeu['nom'] . '">';
                echo '<div class="carousel-caption d-none d-md-block bg-dark-opacity text-white rounded p-1">';
                echo '<h1>' . $rowJeu['nom'] . '</h1>';
                // Ajoutez ici la description du jeu ou tout autre texte souhaité
                echo '</div>';
                echo '</div>';
                $firstGame = false;
                $count++;
            }
            ?>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>

    <!-- Texte de présentation -->
    <div class="mt-4">
        <h2>Bienvenue sur notre site de jeux de société</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac libero et nulla sagittis ullamcorper. Fusce vestibulum, augue in posuere fermentum, ligula velit auctor quam, ac euismod sem elit vel dui.</p>
    </div>
</div>

<?php include 'footer.inc.php'; ?>

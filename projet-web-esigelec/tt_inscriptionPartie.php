<?php
session_start(); // Pour les messages

// Contenu du formulaire :
$idPartie =  htmlentities($_POST['partie_id']);
$idMembre =  htmlentities($_POST['membre_id']);

// Connexion :
require_once("connpdo.php");

// Requête SQL pour l'insertion
$req = "INSERT INTO listemembre1(idMembres, idParties) VALUES (?, ?)";
$ps = $pdo->prepare($req);

// Vérification de la préparation de la requête
if (!$ps) {
    die('Erreur de préparation de la requête.');
}

// Liaison des paramètres
$ps->bindParam(1, $idMembre, PDO::PARAM_INT);
$ps->bindParam(2, $idPartie, PDO::PARAM_INT);

// Exécution de la requête
$resultat = $ps->execute();

// Vérification du résultat de l'exécution
if (!$resultat) {
    die('Erreur d\'exécution de la requête.');
}

// Redirection vers la page d'accueil par exemple :
header('Location: inscriptionPartie.php');
?>

<?php
require_once("roleadmin.php");
$titre = "Modifier Jeu";
include 'header.inc.php';
include 'menuadmin.php';
?>

<div class="container my-5">
    <h1 class="text-center">Modification d'un Jeu</h1>

    <form method="POST" action="tt_modiferjeux.php" enctype="multipart/form-data">
        <div class="row">
            <div class="col-md-6">
                <label for="ancienJeu" class="form-label">Ancien jeu</label>
                <?php
                // Récupérer la valeur de l'ancien jeu depuis la requête GET
                $ancienJeu = isset($_GET['ancienJeu']) ? $_GET['ancienJeu'] : '';
                ?>
                <input type="number" class="form-control" id="ancienJeu" name="id_jeux" value="<?php echo htmlspecialchars($ancienJeu); ?>" required>
            </div>
        </div>

        <div class="row my-3">
            <div class="col-md-6">
                <label for="nouveauNom" class="form-label">Nouveau nom du jeu</label>
                <?php
                // Récupérer la valeur de l'ancien jeu depuis la requête GET
                $nouveauNom = isset($_GET['nouveauNom']) ? $_GET['nouveauNom'] : '';
                ?>
                <input type="text" class="form-control" id="nouveauNom" name="nom" placeholder="Nouveau nom du jeu..." value="<?php echo htmlspecialchars($nouveauNom); ?>" required>
            </div>

            <div class="col-md-6">
                <label for="nouvelleCategorie" class="form-label">Nouvelle Catégorie du jeu</label>
                <?php
                // Récupérer la valeur de l'ancien jeu depuis la requête GET
                $nouvelleCategorie = isset($_GET['nouvelleCategorie']) ? $_GET['nouvelleCategorie'] : '';
                ?>
                <input type="text" class="form-control" id="nouvelleCategorie" name="categorie" placeholder="Nouvelle Catégorie du jeu..." value="<?php echo htmlspecialchars($nouvelleCategorie); ?>" required>
            </div>
        </div>

        <div class="row my-3">
            <div class="col-md-6">
                <label for="nouvelleDescription" class="form-label">Nouvelle Description du jeu</label>
                <?php
                // Récupérer la valeur de l'ancien jeu depuis la requête GET
                $nouvelleDescription = isset($_GET['nouvelleDescription']) ? $_GET['nouvelleDescription'] : '';
                ?>
                <textarea class="form-control" id="nouvelleDescription" name="description" placeholder="Nouvelle Description du jeu..." style="resize: none;" rows="6" required><?php echo htmlspecialchars($nouvelleDescription); ?></textarea>
            </div>

            <div class="col-md-6">
                <label for="userfile" class="form-label">Ajout d'une regle</label>
                <?php
                // Récupérer la valeur de l'ancien jeu depuis la requête GET
                $regles = isset($_GET['regles']) ? $_GET['regles'] : '';
                // Afficher le nom du fichier existant
                if (!empty($regles)) {
                    echo "<p>Fichier actuel : $regles</p>";
                }
                ?>
                <input type="file" name="regles" class="form-control" />
            </div>
        </div>

        <div class="row my-3">
            <div class="col-md-6">
                <label for="userfile" class="form-label">Ajout d'une photo</label>
                <?php
                // Récupérer la valeur de l'ancien jeu depuis la requête GET
                $photo = isset($_GET['photo']) ? $_GET['photo'] : '';
                // Afficher le nom du fichier existant
                if (!empty($photo)) {
                    echo "<p>Fichier actuel : $photo</p>";
                }
                ?>
                <input type="file" name="userfile" class="form-control" />
            </div>
        </div>

        <div class="row my-3">
            <div class="col-md-6">
                <div class="d-grid gap-2 d-md-block">
                    <button class="btn btn-outline-primary" type="submit">Modifier</button>
                </div>
            </div>
        </div>
    </form>
</div>

<?php
include 'footer.inc.php';
?>
